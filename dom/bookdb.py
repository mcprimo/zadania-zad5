#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgi
import cgitb
cgitb.enable()
import os
import datetime

class BookDB():
    def titles(self):
        titles = [dict(id=id, title=database[id]['title']) for id in database.keys()]
        return titles

    def title_info(self, id):
        return database[id]


database = {
    'id1': {'title': 'CherryPy Essentials: Rapid Python Web Application Development',
            'isbn': '978-1904811848',
            'publisher': 'Packt Publishing (March 31, 2007)',
            'author': 'Sylvain Hellegouarch',
            },
    'id2': {'title': 'Python for Software Design: How to Think Like a Computer Scientist',
            'isbn': '978-0521725965',
            'publisher': 'Cambridge University Press; 1 edition (March 16, 2009)',
            'author': 'Allen B. Downey',
            },
    'id3': {'title': 'Foundations of Python Network Programming',
            'isbn': '978-1430230038',
            'publisher': 'Apress; 2 edition (December 21, 2010)',
            'author': 'John Goerzen',
            },
    'id4': {'title': 'Python Cookbook, Second Edition',
            'isbn': '978-0-596-00797-3',
            'publisher': 'O''Reilly Media',
            'author': 'Alex Martelli, Anna Ravenscroft, David Ascher',
            },
    'id5': {'title': 'The Pragmatic Programmer: From Journeyman to Master',
            'isbn': '978-0201616224',
            'publisher': 'Addison-Wesley Professional (October 30, 1999)',
            'author': 'Andrew Hunt, David Thomas',
            },
    }

print "Content-Type: text/html"
print

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
"""

form = cgi.FieldStorage()
id = form.getvalue("id")

Baza=BookDB()

if id is None:
    tytuly=Baza.titles()
    for tytul in tytuly:
        body=body+"""<a href='bookdb.py?id="""+tytul["id"]+"""'>""" +tytul["title"]+ """ </a><br>"""
else:
    try:
        ksiazka=Baza.title_info(id)
        body=body+"""
        <b>Tytuł:</b>&nbsp;"""+ksiazka['title']+"""<br>
        <b>ISBN:</b>&nbsp;"""+ksiazka['isbn']+"""<br>
        <b>Wydawnictwo:</b>&nbsp;"""+ksiazka['publisher']+"""<br>
        <b>Autor:</b>&nbsp;"""+ksiazka['author']
    except:
        body=body+"Brak takiej książki w bazie!"

body=body+"""
    </body>
</html> """

print body,
